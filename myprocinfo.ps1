while ($true) {
    Clear-Host
    Write-Output "1 - Hvem er jeg og hva er navnet p a dette scriptet?"
    Write-Output "2 - Hvor lenge er det siden siste boot?"
    Write-Output "3 - Hvor mange prosesser og tr ̊ader finnes?"
    Write-Output "4 - Hvor mange context switch'er fant sted siste sekund?"
    Write-Output "5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
    Write-Output "6 - Hvor mange interrupts fant sted siste sekund?"
    Write-Output "9 - Avslutt dette scriptet"
    Write-Output ""

    switch (Read-Host "Velg en funksjon: ")
    {
        1 {
            Write-Output "$(whoami) $($MyInvocation.MyCommand.Name)"
        }
        2 {
            $os = Get-CimInstance win32_operatingsystem
            $uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
            Write-Output "Det har vert $($uptime.Days) dager, $($uptime.Hours) timer og $($uptime.Minutes) minutter siden siste boot"
        }
        3 {
            Write-Output "Antall prosesser $((Get-Process).Count) antall tråder $((Get-Process|Select-Object -ExpandProperty Threads).Count)"
        }
        4 {
            $context=$($(Get-Counter -Counter "\System\Context Switches/sec").CounterSamples | Format-Table CookedValue -HideTableHeaders -autosize | Out-String)
            $context = $context -replace "`n","" -replace "`r",""
            Write-Output "Det foregikk $context context switcher siste sekund"
        }
        5 {
            $percentagesUsertime = (gcim Win32_PerfFormattedData_Counters_ProcessorInformation).PercentUserTime
            $percentagesUsertime | ForEach-Object {$sum += $_}
            $sum = [math]::Round([double]$sum / $percentagesUsertime.Length, 2)
            $kernel = 100 - $sum
            Write-Output "$sum% usermode, $kernel% kernelmode"
        }
        6 {
            $interrupts=$($(Get-Counter -Counter "\Processor(_total)\Interrupts/sec").CounterSamples | Format-Table CookedValue  -AutoSize -hidetableheaders | Out-String)
            $interrupts= $interrupts -replace "`n","" -replace "`r",""
            Write-Output "Det var $interrupts interrupts siste sekund"
        }
        9 {
            Exit
        }
    }
Read-Host
Clear-Host
}




