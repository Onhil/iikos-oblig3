[long]$bytes = "$input"

if ( $bytes -gt 1TB ) {
Write-Output "$($bytes)TB"
}
elseif ( $bytes -gt 1GB ){
Write-Output "$(($bytes/1GB))GB"
}
elseif ( $bytes -gt 1MB ){
Write-Output "$(($bytes/1MB))MB"
}
elseif ( $bytes -gt 1KB ){
Write-Output "$(($bytes/1KB))KB"
}
else {
Write-Output $bytes "B"
}