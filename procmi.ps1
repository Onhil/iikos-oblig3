#Eksempel output av Get-Date -UFormat "%Y%m%d-%H%M%S" : 20100314-221523
$format=$(Get-Date -UFormat "%Y%m%d-%H%M%S")

foreach($process in $args){
    $ws = $(Get-process -Id $process | Select-Object -ExpandProperty WorkingSet64 | .\human-readable-bytes.ps1)
    $vms = $(Get-process -Id $process | Select-Object -ExpandProperty VirtualMemorySize64 | .\human-readable-bytes.ps1)
    New-Item ".\$process--$format.meminfo" -ItemType file -Force -Value "******** Minne info om prosess med PID $process ******** `n Total bruk av virtuelt minne: $vms `n St`ørrelse p`å Working Set: $ws"
}